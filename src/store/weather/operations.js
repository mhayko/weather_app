import { fetchWeather, fetchWeatherError, fetchWeatherSuccess } from './actions';
import config from '../../config';

// eslint-disable-next-line import/prefer-default-export
export function getWeatherInfo(cityName) {
    return async (dispatch) => {
        dispatch(fetchWeather());
        try {
            // The WeekContainer will be filled with 5 cards, each representing a day of the week. Because // we’re on a free account,
            // openweathermap API give me weather in different day times,
            // because of it I filter it to 15:00
            const { openWeather, owmKey } = config;
            const response = await fetch(`${openWeather.endpoint}${cityName}&units=metric&APPID=${owmKey.key}`);
            const responseJson = await response.json();
            const weather = responseJson.list.filter((item) => item.dt_txt.includes('15:00:00'));
            dispatch(fetchWeatherSuccess(weather));
        } catch (e) {
            dispatch(fetchWeatherError());
        }
    };
}
