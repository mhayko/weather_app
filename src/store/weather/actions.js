export const types = {
    FETCH_WEATHER: 'FETCH_WEATHER',
    FETCH_WEATHER_SUCCESS: 'FETCH_WEATHER_SUCCESS',
    FETCH_WEATHER_ERROR: 'FETCH_WEATHER_ERROR',
};

export const fetchWeather = () => ({
    type: types.FETCH_WEATHER,
});

export const fetchWeatherSuccess = (payload) => ({
    type: types.FETCH_WEATHER_SUCCESS,
    payload,
});

export const fetchWeatherError = (payload) => ({
    type: types.FETCH_WEATHER_ERROR,
    payload,
});
