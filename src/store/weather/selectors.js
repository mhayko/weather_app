export const getWeather = (state) => state.weather;
export const checkLoading = (state) => state.isLoading;
