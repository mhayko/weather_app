import * as operations from './operations';
import * as actions from './actions';
import * as selectors from './selectors';

export default {
    ...operations,
    ...actions,
    selectors,
};
