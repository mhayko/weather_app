import { types } from './actions';

const initialState = {
    weather: [],
    isLoading: false,
    hasError: false,
};

const reducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case types.FETCH_WEATHER:
            return {
                ...state,
                isLoading: true,
            };
        case types.FETCH_WEATHER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                weather: action.payload,
            };
        case types.FETCH_WEATHER_ERROR:
            return {
                ...state,
                isLoading: false,
                hasError: true,
            };
        default:
            return state;
    }
};

export default reducer;
