import {
    compose,
    createStore,
    applyMiddleware,
} from 'redux';
import thunkMiddleware from 'redux-thunk';

import weatherReducer from './weather/reducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// eslint-disable-next-line max-len
const appStore = () => createStore(weatherReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

export default appStore;
