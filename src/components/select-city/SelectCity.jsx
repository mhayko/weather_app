import React from 'react';
import { map } from 'lodash';
import { PropTypes } from 'prop-types';

import './SelectCity.css';


export default function SelectCity({ onCitySelect, currentCity, cities }) {
    return (
        <select className="select-css" onChange={onCitySelect}>
            {
               currentCity && <option selected value={currentCity}>{currentCity}</option>
            }
            {
                map(cities, (city) => (
                    <option
                        key={city.id}
                        value={city.name}
                    >
                        {city.name}
                    </option>
                ))
            }
        </select>

    );
}

SelectCity.defaultProps = {
    onCitySelect: () => {},
    currentCity: null,
    cities: [],
}

SelectCity.propTypes = {
    onCitySelect: PropTypes.func,
    currentCity: PropTypes.string,
    cities: PropTypes.array,
}
