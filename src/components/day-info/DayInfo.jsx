import React, { Component } from 'react';
import moment from 'moment';
import { first } from 'lodash';
import PropTypes from 'prop-types';


class DayInfo extends Component {
    static getDates(info) {
        let newDate = new Date();
        const weekday = info.dt * 1000;
        newDate = newDate.setTime(weekday);
        const cardDay = moment(newDate).format('dddd');
        const cardDate = moment(newDate).format('MMMM Do, h:mm a');
        return {
            cardDay, cardDate,
        };
    }

    render() {
        const {
            info, selected, onSelectCard, cardIndex, currentCity,
        } = this.props;
        const { main: { temp, temp_max, temp_min } } = info;
        const imageUrl = `http://openweathermap.org/img/wn/${first(info.weather).icon}@2x.png`;
        const dates = DayInfo.getDates(info);

        return (
            <div
                className={selected ? 'card card-selected' : 'card'}
                onClick={() => onSelectCard(cardIndex)}
            >
                <div>
                    <h3 className="card-title mb-sm">{dates.cardDay}</h3>
                    <p className="text-muted m-0">{dates.cardDate}</p>
                </div>
                <img src={imageUrl} alt="" />
                <h2>
                    {Math.round(temp)}
                    {' '}
                    °C
                </h2>
                {
                    selected && (
                        <div className="card-body">
                            <div>
                                <p className="card-text m-0">
                                    Max -
                                    {Math.round(temp_max)}
                                    {' '}
                                    °C
                                </p>
                                <p className="card-text m-0">
                                    Min -
                                    {Math.round(temp_min)}
                                    {' '}
                                    °C
                                </p>
                            </div>
                            <p className="card-text">{first(info.weather).description}</p>
                        </div>
                    )
                }
            </div>
        );
    }
}

DayInfo.defaultProps = {
    selected: false,
    cardIndex: null,
    onSelectCard: () => {},
};

DayInfo.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    info: PropTypes.object.isRequired,
    selected: PropTypes.bool,
    cardIndex: PropTypes.number,
    onSelectCard: PropTypes.func,
};

export default DayInfo;
