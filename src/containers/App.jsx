import React from 'react';
import { connect } from 'react-redux';

import weatherActions from '../store/weather';
import './App.css';
import Weather from './weather/Weather';


function App(props) {
    const { weather, checkLoading, getWeatherInfo } = props;
    return (
        <div className="App">
            <Weather
                weather={weather}
                checkLoading={checkLoading}
                getWeatherInfo={getWeatherInfo}
            />
        </div>
    );
}

const mapStateToProps = (state) => ({
    weather: weatherActions.selectors.getWeather(state),
    checkLoading: weatherActions.selectors.checkLoading(state),
});


const mapDispatchToProps = (dispatch) => ({
    getWeatherInfo: (city) => dispatch(weatherActions.getWeatherInfo(city)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
