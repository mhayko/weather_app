import React, { Component } from 'react';
import { map, first } from 'lodash';
import PropTypes from 'prop-types';
import moment from 'moment';

import DayInfo from '../../components/day-info/DayInfo';
import SelectCity from '../../components/select-city/SelectCity';
import Loader from '../../components/shared/loader/Loader';
import getLocation from '../../services/geolocation';
import config from '../../config';

class Weather extends Component {
    static propTypes = {
        checkLoading: PropTypes.bool,
        // eslint-disable-next-line react/forbid-prop-types
        weather: PropTypes.array.isRequired,
        getWeatherInfo: PropTypes.func.isRequired,
    }

    static defaultProps = {
        checkLoading: false,
    }

    state={
        selectedIndex: null,
        currentCity: null,
        deniedMessage : {
            visible: false,
            msg: 'Please Allow location premmission and reload the page,for showing your city weather'
        }
    }

    constructor(props) {
        super(props);
        this.handleOnCityCelect = this.handleOnCityCelect.bind(this);
        this.handleOnSelectCard = this.handleOnSelectCard.bind(this);
    }

    async componentDidMount() {
        const { cities } = config;
        const currentCity = await getLocation();
        let city;
        if (currentCity) {
            city = currentCity;
            this.setState({ currentCity });
        } else {
            // If premmission denied , we Show firc city in list , and show message
            const { deniedMessage } = this.state;
            deniedMessage.visible = true;
            city = first(cities).name;
            this.setState({deniedMessage});
        }
        this.handleGetWeatherInfo(city)
            .then(() => {
                this.checkSelectedIndex();
            })
    }

    checkSelectedIndex() {
        const { weather } = this.props;
        const newDate = new Date();
        weather.forEach((item, index) => {
            const nowDate = moment(newDate).format("YYYYMMDD");
            const cardDate = moment(item.dt_txt).format("YYYYMMDD");
            if (cardDate === nowDate) {
                this.setState({ selectedIndex: index });
                return nowDate;
            }
        });
    }

    handleGetWeatherInfo(city) {
        const { getWeatherInfo } = this.props;
        return getWeatherInfo(city);
    }

    handleOnSelectCard(index) {
        this.setState({ selectedIndex: index });
    }

    handleOnCityCelect(e) {
        e.preventDefault();
        const { value } = e.target;
        this.handleGetWeatherInfo(value);
    }

    render() {
        const { checkLoading, weather } = this.props;
        const { selectedIndex, currentCity, deniedMessage } = this.state;
        const { cities } = config;
        return (
            <div className="wrapper">
                <h1>Weather for BrainStorm</h1>
                <div className="search-city">
                    <SelectCity
                        onCitySelect={this.handleOnCityCelect}
                        currentCity={currentCity}
                        cities={cities}
                    />
                </div>
                {
                    deniedMessage.visible && <p className="err">{deniedMessage.msg}</p>
                }
                <div className="cards-parrent">
                    {
                        !checkLoading ? map(weather, (item, index) => (
                            <div key={index}>
                                <DayInfo
                                    info={item}
                                    selected={selectedIndex === index}
                                    cardIndex={index}
                                    onSelectCard={this.handleOnSelectCard}
                                />
                            </div>
                        )) : <Loader />

                    }
                </div>
            </div>
        );
    }
}


export default Weather;
