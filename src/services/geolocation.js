import config from '../config';

function getPosition() {
    if (!navigator.geolocation) return 'denied';
    return new Promise((res, rej) => {
        navigator.geolocation.getCurrentPosition(res, rej);
    });
}

async function getLocation() {
    let coords;
    let access;
    await getPosition()
        .then(res => {
            access = true;
            coords = res.coords;
        })
        .catch(err => {
            access = false;
            console.log(err);
        });
    if (!access) return false;
    const { latitude: lat, longitude: lon } = coords;
    const data = await fetch(
        `${config.reverseGeo.endpoint}${lon}%2C${lat}&langCode=fr&outSR=&forStorage=false&f=pjson`,
    );
    const dataJson = await data.json();
    const currentCity = dataJson.address.Region;
    return currentCity;
}

export default getLocation;
